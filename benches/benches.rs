#![feature(test)]
extern crate test;

#[macro_use]
extern crate lazy_static;

use toy_renderer::geometry::P3;
use toy_renderer::obj::with_normals;
use toy_renderer::rasterizer::{BoundingBox, LineSweeping, Rasterizer};
use toy_renderer::rendering::{Rendering, RGB};
use toy_renderer::shader::fragment::{
    FragmentShader, MonochromaticScale, RainbowScale, ScaleGouraud,
    TexturedGouraud,
};
use toy_renderer::shader::vertex::{Transforming, VertexShader};

use image::imageops;
use image::io::Reader;
use image::RgbImage;
use obj::Obj;
use std::env;
use test::Bencher;

// The bounding box rasterizer seems to be faster for smaller rendering
// sizes and the line sweeper rasterizer for larger ones...

const HEIGHT: u32 = 600;
const WIDTH: u32 = 800;
const CAMERA: P3<f32> = P3(6.0, 8.0, -10.0);
const LIGHT: P3<f32> = P3(0.0, -0.6, 0.8);
const COLOR: RGB = RGB(255, 0, 0);

lazy_static! {
    static ref OBJ: Obj = {
        let dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let path = format!("{}/{}", dir, "models/spot/spot.obj");
        with_normals(Obj::load(path).unwrap())
    };
    static ref TEXTURE: RgbImage = {
        let dir = env::var("CARGO_MANIFEST_DIR").unwrap();
        let path = format!("{}/{}", dir, "models/spot/spot_texture.png");
        let mut texture =
            Reader::open(path).unwrap().decode().unwrap().into_rgb8();
        imageops::flip_vertical_in_place(&mut texture);
        texture
    };
}

fn benchmark_renderer<
    D,
    R: Rasterizer,
    V: VertexShader<D>,
    F: FragmentShader<D>,
>(
    b: &mut Bencher,
    rasterizer: R,
    vertex_shader: V,
    fragment_shader: F,
) {
    let mut rendering = Rendering::new(WIDTH, HEIGHT);

    b.iter(|| {
        rendering.render_obj(
            &OBJ,
            &rasterizer,
            &vertex_shader,
            &fragment_shader,
        );
    });
}

#[bench]
fn spot_line_sweeping_textured(b: &mut Bencher) {
    benchmark_renderer(
        b,
        LineSweeping(),
        Transforming::new(CAMERA, WIDTH, HEIGHT),
        TexturedGouraud::new(&TEXTURE, LIGHT),
    )
}

#[bench]
fn spot_bounding_box_textured(b: &mut Bencher) {
    benchmark_renderer(
        b,
        BoundingBox(),
        Transforming::new(CAMERA, WIDTH, HEIGHT),
        TexturedGouraud::new(&TEXTURE, LIGHT),
    )
}

#[bench]
fn spot_bounding_box_monochromatic(b: &mut Bencher) {
    benchmark_renderer(
        b,
        BoundingBox(),
        Transforming::new(CAMERA, WIDTH, HEIGHT),
        ScaleGouraud::new(MonochromaticScale(COLOR), LIGHT),
    )
}

#[bench]
fn spot_bounding_box_rainbow(b: &mut Bencher) {
    benchmark_renderer(
        b,
        BoundingBox(),
        Transforming::new(CAMERA, WIDTH, HEIGHT),
        ScaleGouraud::new(RainbowScale(), LIGHT),
    )
}

fn benchmark_threaded_renderer<
    D: Clone + Send + Sync,
    R: Sync + Rasterizer,
    V: Sync + VertexShader<D>,
    F: Sync + FragmentShader<D>,
>(
    b: &mut Bencher,
    rasterizer: R,
    vertex_shader: V,
    fragment_shader: F,
) {
    let mut rendering = Rendering::new(WIDTH, HEIGHT);

    b.iter(|| {
        rendering.render_obj_threaded(
            &OBJ,
            &rasterizer,
            &vertex_shader,
            &fragment_shader,
        );
    });
}

#[bench]
fn spot_line_sweeping_textured_threaded(b: &mut Bencher) {
    benchmark_threaded_renderer(
        b,
        LineSweeping(),
        Transforming::new(CAMERA, WIDTH, HEIGHT),
        TexturedGouraud::new(&TEXTURE, LIGHT),
    )
}

#[bench]
fn spot_bounding_box_textured_threaded(b: &mut Bencher) {
    benchmark_threaded_renderer(
        b,
        BoundingBox(),
        Transforming::new(CAMERA, WIDTH, HEIGHT),
        TexturedGouraud::new(&TEXTURE, LIGHT),
    )
}
