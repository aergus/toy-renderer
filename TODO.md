* Add more documentation.
* Switch to slices from "homogeneous" tuples?
* Remove as many `unwrap`s as possible.
  - Do something about missing textures.
  - Handle IO errors in `main.rs` more explicitly?
* Use macros to generate:
  - tuple parsers
  - iterator functions for "geometric types"
  - benchmarks?
* Review integer casts (and add warnings about them?).
* Experiment with different "passing styles" for fragment shaders
("by reference", "by value", "interpolated" etc.).
* Try to get rid of boxed iterators in "raw" rasterizers.
* Analysis of the line sweeping algorithm:
  - rounding issues?
  - performance
* Analyze the performance of threaded rendering.
* Live rendering?
