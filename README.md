# Toy Renderer

This is a toy software renderer implemented by more or less following
[Dmitry V. Sokolov's tiny renderer series][tinyrenderer].

[tinyrenderer]: https://github.com/ssloy/tinyrenderer/wiki
