pub mod transforming;
pub use transforming::Transforming;

use super::super::geometry::P4;

use obj::{IndexTuple, ObjData};

pub trait VertexShader<D> {
    fn vertex(&self, obj_data: &ObjData, index: IndexTuple) -> (P4<f32>, D);
}
