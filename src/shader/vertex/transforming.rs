use super::super::super::geometry::{Transformation, P2, P3, P4};
use super::super::data::{Normal, TextureAndNormal};
use super::VertexShader;

use obj::{IndexTuple, ObjData};
use std::cmp;

#[derive(Clone, PartialEq, Debug)]
pub struct Transforming(Transformation);

impl VertexShader<TextureAndNormal> for Transforming {
    fn vertex(
        &self,
        obj_data: &ObjData,
        index: IndexTuple,
    ) -> (P4<f32>, TextureAndNormal) {
        let position = self.0 * P4::from(P3::from(obj_data.position[index.0]));
        let texture = obj_data.texture[index.1.unwrap()];
        let texture = P2(texture[0], texture[1]);
        let normal = P3::from(obj_data.normal[index.2.unwrap()]);
        (position, TextureAndNormal { texture, normal })
    }
}

impl VertexShader<Normal> for Transforming {
    fn vertex(
        &self,
        obj_data: &ObjData,
        index: IndexTuple,
    ) -> (P4<f32>, Normal) {
        let position = self.0 * P4::from(P3::from(obj_data.position[index.0]));
        let normal = P3::from(obj_data.normal[index.2.unwrap()]);
        (position, normal)
    }
}

impl Transforming {
    pub fn new(camera: P3<f32>, width: u32, height: u32) -> Self {
        let half_width = width as f32 / 2.0;
        let half_height = height as f32 / 2.0;
        let factor = cmp::min(width, height) as f32 / 2.0;
        let transformation =
            Transformation::translation(P3(half_width, half_height, 0.0))
                * Transformation::scaling(factor)
                * Transformation::projection(camera.2)
                * Transformation::view(camera, P3(0.0, 1.0, 0.0));
        Transforming(transformation)
    }
}
