use super::super::super::geometry::{Barycentric, Triangle, P3};

use super::super::super::rendering::{BLACK, RGB};
use super::super::data::Normal;
use super::FragmentShader;

use palette::encoding::srgb::Srgb;
use palette::{Hsv, IntoColor};

pub struct ScaleGouraud<S> {
    scale: S,
    light_direction: P3<f32>,
}

impl<S: ColorScale> FragmentShader<Normal> for ScaleGouraud<S> {
    fn fragment(
        &self,
        triangle_data: &Triangle<Normal>,
        coords: Barycentric,
    ) -> Option<RGB> {
        self.scale
            .get_color(triangle_data.intensity_at(self.light_direction, coords))
    }
}

impl<S> ScaleGouraud<S> {
    pub fn new(scale: S, mut light_direction: P3<f32>) -> Self {
        light_direction.normalize();
        ScaleGouraud {
            scale,
            light_direction,
        }
    }
}

pub trait ColorScale {
    fn get_color(&self, intensity: f32) -> Option<RGB>;
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct MonochromaticScale(pub RGB);

impl ColorScale for MonochromaticScale {
    fn get_color(&self, intensity: f32) -> Option<RGB> {
        if intensity <= 0.0 {
            Some(BLACK)
        } else {
            Some(self.0 * intensity)
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct RainbowScale();

impl ColorScale for RainbowScale {
    fn get_color(&self, intensity: f32) -> Option<RGB> {
        let (r, g, b): (u8, u8, u8) =
            Hsv::new(150.0 * (intensity + 1.0), 1.0, 1.0)
                .into_rgb::<Srgb>()
                .into_format()
                .into_components();
        Some(RGB(r, g, b))
    }
}
