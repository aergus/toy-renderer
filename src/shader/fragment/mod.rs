pub mod scale_gouraud;
pub use scale_gouraud::{MonochromaticScale, RainbowScale, ScaleGouraud};
pub mod textured_gouraud;
pub use textured_gouraud::TexturedGouraud;

use super::super::geometry::{Barycentric, Triangle};
use super::super::rendering::RGB;

pub trait FragmentShader<D> {
    fn fragment(
        &self,
        triangle_data: &Triangle<D>,
        coords: Barycentric,
    ) -> Option<RGB>;
}
