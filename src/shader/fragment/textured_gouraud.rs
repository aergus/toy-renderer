use super::super::super::geometry::{Barycentric, Triangle, P3};

use super::super::super::rendering::{BLACK, RGB};
use super::super::data::TextureAndNormal;
use super::FragmentShader;

use image::RgbImage;
use std::cmp;

pub struct TexturedGouraud<'a> {
    texture: &'a RgbImage,
    light_direction: P3<f32>,
}

impl FragmentShader<TextureAndNormal> for TexturedGouraud<'_> {
    fn fragment(
        &self,
        triangle_data: &Triangle<TextureAndNormal>,
        coords: Barycentric,
    ) -> Option<RGB> {
        let intensity = triangle_data
            .map(|d| d.normal)
            .intensity_at(self.light_direction, coords);
        if intensity <= 0.0 {
            Some(BLACK)
        } else {
            let texture_position =
                triangle_data.map(|d| d.texture).interpolate(coords);
            let texture_x = cmp::max(
                0,
                cmp::min(
                    (texture_position.0 * self.texture.width() as f32) as i32,
                    self.texture.width() as i32 - 1,
                ),
            ) as u32;
            let texture_y = cmp::max(
                0,
                cmp::min(
                    (texture_position.1 * self.texture.height() as f32) as i32,
                    self.texture.height() as i32 - 1,
                ),
            ) as u32;

            Some(
                RGB::from(*self.texture.get_pixel(texture_x, texture_y))
                    * intensity,
            )
        }
    }
}

impl<'a> TexturedGouraud<'a> {
    pub fn new(texture: &'a RgbImage, mut light_direction: P3<f32>) -> Self {
        light_direction.normalize();
        TexturedGouraud {
            light_direction,
            texture,
        }
    }
}
