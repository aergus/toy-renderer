use super::super::geometry::{Barycentric, Triangle, P2, P3};

use derive_more::{Add, Mul};

pub type Normal = P3<f32>;

impl Triangle<Normal> {
    pub fn intensity_at(
        &self,
        light_direction: P3<f32>,
        coords: Barycentric,
    ) -> f32 {
        self.interpolate(coords).normalized().dot(light_direction)
    }
}

#[derive(Clone, Copy, PartialEq, Debug, Add, Mul)]
pub struct TextureAndNormal {
    pub texture: P2<f32>,
    pub normal: P3<f32>,
}
