use super::RGB;

use rand::Rng;

impl RGB {
    pub fn random() -> Self {
        let mut rng = rand::thread_rng();
        RGB(
            rng.gen_range(63, 255),
            rng.gen_range(63, 255),
            rng.gen_range(63, 255),
        )
    }
}
