pub mod test;

use super::geometry::{RenderingP, Triangle, P2};
use super::matrix::Matrix;
use super::rasterizer::{RasterizedP, Rasterizer};
use super::shader::fragment::FragmentShader;
use super::shader::vertex::VertexShader;

use image::imageops;
use image::{ImageResult, Rgb, RgbImage};
use obj::Obj;
use std::convert::From;
use std::ops::Mul;
use std::path;
use std::sync::mpsc::channel;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct RGB(pub u8, pub u8, pub u8);

impl From<Rgb<u8>> for RGB {
    fn from(color: Rgb<u8>) -> RGB {
        RGB(color[0], color[1], color[2])
    }
}

impl RGB {
    pub fn inverted(&self) -> Self {
        RGB(255 - self.0, 255 - self.1, 255 - self.2)
    }
}

impl Mul<f32> for RGB {
    type Output = RGB;

    fn mul(self, rhs: f32) -> Self::Output {
        RGB(
            (self.0 as f32 * rhs) as u8,
            (self.1 as f32 * rhs) as u8,
            (self.2 as f32 * rhs) as u8,
        )
    }
}

pub const BLACK: RGB = RGB(0, 0, 0);
pub const RED: RGB = RGB(255, 0, 0);
pub const GREEN: RGB = RGB(0, 255, 0);
pub const BLUE: RGB = RGB(0, 0, 255);
pub const WHITE: RGB = RGB(255, 255, 255);

/// Tracks the rendered image, its size and a z-buffer.
#[derive(Clone, Debug)]
pub struct Rendering {
    corner: P2<i32>,
    image: RgbImage,
    z_buffer: Matrix<f32>,
}

impl Rendering {
    pub fn new(width: u32, height: u32) -> Self {
        let image = RgbImage::new(width, height);
        let z_buffer = Matrix::with_default(width, height, f32::NEG_INFINITY);
        Rendering {
            corner: P2(width as i32 - 1, height as i32 - 1),
            image,
            z_buffer,
        }
    }

    /// Returns the point with the maximal x and y coordinates that is still
    /// within the rendering frame.
    pub fn get_corner(&self) -> P2<i32> {
        self.corner
    }

    pub fn get_z_buffer(&self, x: u32, y: u32) -> f32 {
        self.z_buffer[(x, y)]
    }

    /// Puts a pixel of the given color at the given position and updates
    /// the z-buffer at the given position with the given value.
    pub fn put_pixel(&mut self, P2(x, y): P2<u32>, z: f32, color: RGB) {
        self.z_buffer[(x, y)] = z;
        self.image.put_pixel(x, y, Rgb([color.0, color.1, color.2]))
    }

    /// Renders a model represented by an [Obj].
    ///
    /// # The rendering "pipeline"
    ///
    /// For every triangle in the in the model, this function does the
    /// following.
    ///
    /// 1. Applies the given [VertexShader] to its vertices, obtaining
    /// transformed vertices and additional data `D` (e.g. texture coordinates)
    /// we might need for the rendering.
    /// 2. Feeds the transformed triangle into the given [Rasterizer],
    /// producing an iterator of [RasterizedP]s.
    /// 3. For each such point, checks the z-buffer and if the point should be
    /// rendered, passes its barycentric coordinates with respect to the
    /// current triangle and the data generated by the vertex shader to
    /// the given [FragmentShader].
    /// 4. If the fragment shader yields a color, renders the point using
    /// [Rendering::put_pixel].
    pub fn render_obj<
        D,
        R: Rasterizer,
        V: VertexShader<D>,
        F: FragmentShader<D>,
    >(
        &mut self,
        obj: &Obj,
        rasterizer: &R,
        vertex_shader: &V,
        fragment_shader: &F,
    ) {
        let data = &obj.data;
        for object in data.objects.iter() {
            for group in object.groups.iter() {
                for polygon in group.polys.iter() {
                    let l = polygon.0.len();
                    for i in 1..l - 1 {
                        // TODO: Find a better way of doing the
                        // "array-of-structs to struct-of-arrays conversion".
                        let triangle = Triangle(
                            vertex_shader.vertex(data, (polygon.0)[0]),
                            vertex_shader.vertex(data, (polygon.0)[i]),
                            vertex_shader.vertex(data, (polygon.0)[i + 1]),
                        );
                        let (triangle, triangle_data) = (
                            Triangle(
                                triangle.0 .0,
                                triangle.1 .0,
                                triangle.2 .0,
                            ),
                            Triangle(
                                triangle.0 .1,
                                triangle.1 .1,
                                triangle.2 .1,
                            ),
                        );
                        let rendering_triangle = triangle.map(|p| {
                            p.perspective_projection().for_rendering()
                        });
                        for RasterizedP {
                            rendering_p: RenderingP(x, y, z),
                            coords,
                        } in rasterizer.rasterize(
                            triangle,
                            rendering_triangle,
                            self.corner,
                        ) {
                            if self.z_buffer[(x as u32, y as u32)] < z {
                                // Using the `or_else` variant seems to ensure
                                // "laziness"...
                                let coords = coords.unwrap_or_else(|| {
                                    rendering_triangle
                                        .map(|p| p.project())
                                        .barycentric(P2(x, y))
                                        .correct_perspective(triangle)
                                });
                                if let Some(color) = fragment_shader
                                    .fragment(&triangle_data, coords)
                                {
                                    self.put_pixel(
                                        P2(x as u32, y as u32),
                                        z,
                                        color,
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /// A version [Rendering::render_obj] that uses a thread pool to
    /// parallelize fragment shaders.
    ///
    /// It doesn't seem to perform better than the single-threaded version.
    pub fn render_obj_threaded<
        D: Clone + Send + Sync,
        R: Sync + Rasterizer,
        V: Sync + VertexShader<D>,
        F: Sync + FragmentShader<D>,
    >(
        &mut self,
        obj: &Obj,
        rasterizer: &R,
        vertex_shader: &V,
        fragment_shader: &F,
    ) {
        let data = &obj.data;

        rayon::scope(|s| {
            for object in data.objects.iter() {
                for group in object.groups.iter() {
                    for polygon in group.polys.iter() {
                        let (tx, rx) = channel();

                        let l = polygon.0.len();
                        for i in 1..l - 1 {
                            // TODO: Find a better way of doing the
                            // "array-of-structs to struct-of-arrays
                            // conversion".
                            let triangle = Triangle(
                                vertex_shader.vertex(data, (polygon.0)[0]),
                                vertex_shader.vertex(data, (polygon.0)[i]),
                                vertex_shader.vertex(data, (polygon.0)[i + 1]),
                            );
                            let (triangle, triangle_data) = (
                                Triangle(
                                    triangle.0 .0,
                                    triangle.1 .0,
                                    triangle.2 .0,
                                ),
                                Triangle(
                                    triangle.0 .1,
                                    triangle.1 .1,
                                    triangle.2 .1,
                                ),
                            );
                            let rendering_triangle = triangle.map(|p| {
                                p.perspective_projection().for_rendering()
                            });
                            for RasterizedP {
                                rendering_p: RenderingP(x, y, z),
                                coords,
                            } in rasterizer.rasterize(
                                triangle,
                                rendering_triangle,
                                self.corner,
                            ) {
                                if self.z_buffer[(x as u32, y as u32)] < z {
                                    let tx = tx.clone();
                                    let triangle_data = triangle_data.clone();

                                    s.spawn(move |_| {
                                        // Using the `or_else` variant seems
                                        // to ensure "laziness"...
                                        let coords =
                                            coords.unwrap_or_else(|| {
                                                rendering_triangle
                                                    .map(|p| p.project())
                                                    .barycentric(P2(x, y))
                                                    .correct_perspective(
                                                        triangle,
                                                    )
                                            });
                                        if let Some(color) = fragment_shader
                                            .fragment(&triangle_data, coords)
                                        {
                                            tx.send((
                                                P2(x as u32, y as u32),
                                                z,
                                                color,
                                            ))
                                            .unwrap()
                                        }
                                    });
                                }
                            }
                        }

                        drop(tx);

                        for (p, z, color) in rx.iter() {
                            self.put_pixel(p, z, color);
                        }
                    }
                }
            }
        });
    }

    pub fn save<Q: AsRef<path::Path>>(&self, path: Q) -> ImageResult<()> {
        imageops::flip_vertical(&self.image).save(path)
    }
}
