use std::ops::{Index, IndexMut};

// Reinventing the wheel to get familiar with the traits involved, in
// "production" one should probably use a library like:
// * https://crates.io/crates/grid
// * https://crates.io/crates/toodee
// * https://crates.io/crates/nalgebra

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Matrix<T> {
    width: usize,
    data: Vec<T>,
}

impl<T> Matrix<T> {
    pub fn new(width: u32, height: u32) -> Self {
        let data = Vec::with_capacity(width as usize * height as usize);
        Matrix {
            width: width as usize,
            data,
        }
    }
}

impl<T: Clone> Matrix<T> {
    pub fn with_default(width: u32, height: u32, val: T) -> Self {
        let data = vec![val; width as usize * height as usize];
        Matrix {
            width: width as usize,
            data,
        }
    }
}

impl<T> Index<(u32, u32)> for Matrix<T> {
    type Output = T;
    fn index(&self, (i, j): (u32, u32)) -> &Self::Output {
        self.data.index(i as usize + j as usize * self.width)
    }
}

impl<T> IndexMut<(u32, u32)> for Matrix<T> {
    fn index_mut(&mut self, (i, j): (u32, u32)) -> &mut Self::Output {
        self.data.index_mut(i as usize + j as usize * self.width)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Matrix4<T>([T; 16]);

impl<T> Index<(usize, usize)> for Matrix4<T> {
    type Output = T;
    fn index(&self, (i, j): (usize, usize)) -> &Self::Output {
        self.0.index(i * 4 + j)
    }
}

impl<T> IndexMut<(usize, usize)> for Matrix4<T> {
    fn index_mut(&mut self, (i, j): (usize, usize)) -> &mut Self::Output {
        self.0.index_mut(i * 4 + j)
    }
}

pub const ZERO4: Matrix4<f32> = Matrix4([0.0; 16]);
#[rustfmt::skip]
pub const ID4: Matrix4<f32> = Matrix4([
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0,
]);

impl Matrix4<f32> {
    pub fn transpose(&mut self) {
        self.0.swap(1, 4);
        self.0.swap(2, 8);
        self.0.swap(3, 12);
        self.0.swap(6, 9);
        self.0.swap(7, 13);
        self.0.swap(11, 14);
    }
    pub fn transposed(&mut self) -> Self {
        self.transpose();
        *self
    }
    // This hack is adapted from the following SO thread:
    // https://stackoverflow.com/q/1148309
    pub fn inverse(self) -> Option<Self> {
        let mut inverse = [
            self.0[5] * self.0[10] * self.0[15]
                - self.0[5] * self.0[11] * self.0[14]
                - self.0[9] * self.0[6] * self.0[15]
                + self.0[9] * self.0[7] * self.0[14]
                + self.0[13] * self.0[6] * self.0[11]
                - self.0[13] * self.0[7] * self.0[10],
            -self.0[1] * self.0[10] * self.0[15]
                + self.0[1] * self.0[11] * self.0[14]
                + self.0[9] * self.0[2] * self.0[15]
                - self.0[9] * self.0[3] * self.0[14]
                - self.0[13] * self.0[2] * self.0[11]
                + self.0[13] * self.0[3] * self.0[10],
            self.0[1] * self.0[6] * self.0[15]
                - self.0[1] * self.0[7] * self.0[14]
                - self.0[5] * self.0[2] * self.0[15]
                + self.0[5] * self.0[3] * self.0[14]
                + self.0[13] * self.0[2] * self.0[7]
                - self.0[13] * self.0[3] * self.0[6],
            -self.0[1] * self.0[6] * self.0[11]
                + self.0[1] * self.0[7] * self.0[10]
                + self.0[5] * self.0[2] * self.0[11]
                - self.0[5] * self.0[3] * self.0[10]
                - self.0[9] * self.0[2] * self.0[7]
                + self.0[9] * self.0[3] * self.0[6],
            -self.0[4] * self.0[10] * self.0[15]
                + self.0[4] * self.0[11] * self.0[14]
                + self.0[8] * self.0[6] * self.0[15]
                - self.0[8] * self.0[7] * self.0[14]
                - self.0[12] * self.0[6] * self.0[11]
                + self.0[12] * self.0[7] * self.0[10],
            self.0[0] * self.0[10] * self.0[15]
                - self.0[0] * self.0[11] * self.0[14]
                - self.0[8] * self.0[2] * self.0[15]
                + self.0[8] * self.0[3] * self.0[14]
                + self.0[12] * self.0[2] * self.0[11]
                - self.0[12] * self.0[3] * self.0[10],
            -self.0[0] * self.0[6] * self.0[15]
                + self.0[0] * self.0[7] * self.0[14]
                + self.0[4] * self.0[2] * self.0[15]
                - self.0[4] * self.0[3] * self.0[14]
                - self.0[12] * self.0[2] * self.0[7]
                + self.0[12] * self.0[3] * self.0[6],
            self.0[0] * self.0[6] * self.0[11]
                - self.0[0] * self.0[7] * self.0[10]
                - self.0[4] * self.0[2] * self.0[11]
                + self.0[4] * self.0[3] * self.0[10]
                + self.0[8] * self.0[2] * self.0[7]
                - self.0[8] * self.0[3] * self.0[6],
            self.0[4] * self.0[9] * self.0[15]
                - self.0[4] * self.0[11] * self.0[13]
                - self.0[8] * self.0[5] * self.0[15]
                + self.0[8] * self.0[7] * self.0[13]
                + self.0[12] * self.0[5] * self.0[11]
                - self.0[12] * self.0[7] * self.0[9],
            -self.0[0] * self.0[9] * self.0[15]
                + self.0[0] * self.0[11] * self.0[13]
                + self.0[8] * self.0[1] * self.0[15]
                - self.0[8] * self.0[3] * self.0[13]
                - self.0[12] * self.0[1] * self.0[11]
                + self.0[12] * self.0[3] * self.0[9],
            self.0[0] * self.0[5] * self.0[15]
                - self.0[0] * self.0[7] * self.0[13]
                - self.0[4] * self.0[1] * self.0[15]
                + self.0[4] * self.0[3] * self.0[13]
                + self.0[12] * self.0[1] * self.0[7]
                - self.0[12] * self.0[3] * self.0[5],
            -self.0[0] * self.0[5] * self.0[11]
                + self.0[0] * self.0[7] * self.0[9]
                + self.0[4] * self.0[1] * self.0[11]
                - self.0[4] * self.0[3] * self.0[9]
                - self.0[8] * self.0[1] * self.0[7]
                + self.0[8] * self.0[3] * self.0[5],
            -self.0[4] * self.0[9] * self.0[14]
                + self.0[4] * self.0[10] * self.0[13]
                + self.0[8] * self.0[5] * self.0[14]
                - self.0[8] * self.0[6] * self.0[13]
                - self.0[12] * self.0[5] * self.0[10]
                + self.0[12] * self.0[6] * self.0[9],
            self.0[0] * self.0[9] * self.0[14]
                - self.0[0] * self.0[10] * self.0[13]
                - self.0[8] * self.0[1] * self.0[14]
                + self.0[8] * self.0[2] * self.0[13]
                + self.0[12] * self.0[1] * self.0[10]
                - self.0[12] * self.0[2] * self.0[9],
            -self.0[0] * self.0[5] * self.0[14]
                + self.0[0] * self.0[6] * self.0[13]
                + self.0[4] * self.0[1] * self.0[14]
                - self.0[4] * self.0[2] * self.0[13]
                - self.0[12] * self.0[1] * self.0[6]
                + self.0[12] * self.0[2] * self.0[5],
            self.0[0] * self.0[5] * self.0[10]
                - self.0[0] * self.0[6] * self.0[9]
                - self.0[4] * self.0[1] * self.0[10]
                + self.0[4] * self.0[2] * self.0[9]
                + self.0[8] * self.0[1] * self.0[6]
                - self.0[8] * self.0[2] * self.0[5],
        ];

        let determinant = self.0[0] * inverse[0]
            + self.0[1] * inverse[4]
            + self.0[2] * inverse[8]
            + self.0[3] * inverse[12];

        if determinant == 0.0 {
            None
        } else {
            for c in inverse.iter_mut() {
                *c /= determinant;
            }
            Some(Matrix4(inverse))
        }
    }
}
