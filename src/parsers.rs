use super::geometry::{P2, P3, P4};
use super::rendering::RGB;

use derive_more::Display;
use std::convert::From;
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Clone, Copy, PartialEq, Eq, Debug, Display)]
pub enum ParseTupleError<E> {
    #[display(fmt = "missing left parenthesis")]
    MissingLeftParenthesis,
    #[display(fmt = "missing right parenthesis")]
    MissingRightParenthesis,
    #[display(fmt = "unsuitable length: {}", _0)]
    UnsuitableLength(usize),
    #[display(fmt = "error while parsing a coefficient: {}", _0)]
    CoefficientError(E),
}

impl<E> From<E> for ParseTupleError<E> {
    fn from(e: E) -> ParseTupleError<E> {
        ParseTupleError::CoefficientError(e)
    }
}

fn tokenize_tuple<E>(s: &str) -> Result<Vec<&str>, ParseTupleError<E>> {
    if !s.starts_with('(') {
        Err(ParseTupleError::MissingLeftParenthesis)
    } else if !s.ends_with(')') {
        Err(ParseTupleError::MissingRightParenthesis)
    } else {
        Ok(s[1..s.len() - 1].split(',').map(|u| u.trim()).collect())
    }
}

impl<T: FromStr<Err = E>, E> FromStr for P2<T> {
    type Err = ParseTupleError<E>;

    fn from_str(s: &str) -> Result<P2<T>, Self::Err> {
        let coefficient_strings = tokenize_tuple(s)?;
        let l = coefficient_strings.len();
        if l != 2 {
            Err(ParseTupleError::UnsuitableLength(l))
        } else {
            let first = FromStr::from_str(coefficient_strings[0])?;
            let second = FromStr::from_str(coefficient_strings[1])?;
            Ok(P2(first, second))
        }
    }
}

impl<T: FromStr<Err = E>, E> FromStr for P3<T> {
    type Err = ParseTupleError<E>;

    fn from_str(s: &str) -> Result<P3<T>, Self::Err> {
        let coefficient_strings = tokenize_tuple(s)?;
        let l = coefficient_strings.len();
        if l != 3 {
            Err(ParseTupleError::UnsuitableLength(l))
        } else {
            let first = FromStr::from_str(coefficient_strings[0])?;
            let second = FromStr::from_str(coefficient_strings[1])?;
            let third = FromStr::from_str(coefficient_strings[2])?;
            Ok(P3(first, second, third))
        }
    }
}

impl<T: FromStr<Err = E>, E> FromStr for P4<T> {
    type Err = ParseTupleError<E>;

    fn from_str(s: &str) -> Result<P4<T>, Self::Err> {
        let coefficient_strings = tokenize_tuple(s)?;
        let l = coefficient_strings.len();
        if l != 4 {
            Err(ParseTupleError::UnsuitableLength(l))
        } else {
            let first = FromStr::from_str(coefficient_strings[0])?;
            let second = FromStr::from_str(coefficient_strings[1])?;
            let third = FromStr::from_str(coefficient_strings[2])?;
            let fourth = FromStr::from_str(coefficient_strings[3])?;
            Ok(P4(first, second, third, fourth))
        }
    }
}

impl FromStr for RGB {
    type Err = ParseTupleError<ParseIntError>;

    fn from_str(s: &str) -> Result<RGB, Self::Err> {
        let coefficient_strings = tokenize_tuple(s)?;
        let l = coefficient_strings.len();
        if l != 3 {
            Err(ParseTupleError::UnsuitableLength(l))
        } else {
            let red = FromStr::from_str(coefficient_strings[0])?;
            let green = FromStr::from_str(coefficient_strings[1])?;
            let blue = FromStr::from_str(coefficient_strings[2])?;
            Ok(RGB(red, green, blue))
        }
    }
}
