use super::super::geometry::{
    bounding_box, Barycentric, RenderingP, Triangle, P2, P4,
};
use super::{RasterizedP, Rasterizer};

/// A [Rasterizer] that checks which points in the bounding box of the
/// triangle are actually on the triangle by calculating their barycentric
/// coordinates and emits those that are.
pub struct BoundingBox();

impl Rasterizer for BoundingBox {
    fn rasterize(
        &self,
        triangle: Triangle<P4<f32>>,
        rendering_triangle: Triangle<RenderingP>,
        corner: P2<i32>,
    ) -> Box<dyn Iterator<Item = RasterizedP>> {
        let screen_triangle = rendering_triangle.map(|p| p.project());
        let (lower_left, upper_right) =
            bounding_box(screen_triangle, P2(0, 0), corner);
        Box::new(
            (lower_left.0..=upper_right.0)
                .flat_map(move |x| {
                    (lower_left.1..=upper_right.1).map(move |y| (x, y))
                })
                .filter_map(move |(x, y)| {
                    let (nums, denom) =
                        screen_triangle.scaled_barycentric(P2(x, y));
                    if denom != 0 && nums.0 >= 0 && nums.1 >= 0 && nums.2 >= 0 {
                        let coords = Barycentric::from_scaled(nums, denom)
                            .correct_perspective(triangle);
                        let z = coords.0 * triangle.0 .2
                            + coords.1 * triangle.1 .2
                            + coords.2 * triangle.2 .2;
                        Some(RasterizedP {
                            rendering_p: RenderingP(x, y, z),
                            coords: Some(coords),
                        })
                    } else {
                        None
                    }
                }),
        )
    }
}
