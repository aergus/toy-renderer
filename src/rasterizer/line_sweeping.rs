use super::super::geometry::parametrization::ParametrizedLine;
use super::super::geometry::{Line, RenderingP, Triangle, P2, P4};
use super::{RasterizedP, Rasterizer};

use std::cmp;
use std::iter;

// This iterator-based approach seems to be slower than the more direct version
// introduced in the commit `5c7e284f212cb321cca1341b45e4fd1e7e86c4ad`,
// but it's easier to parallelize.

/// A [Rasterizer] that "sweeps" between the upper and lower edges of the
/// triangle from left to right.
pub struct LineSweeping();

impl Rasterizer for LineSweeping {
    fn rasterize(
        &self,
        _: Triangle<P4<f32>>,
        rendering_triangle: Triangle<RenderingP>,
        corner: P2<i32>,
    ) -> Box<dyn Iterator<Item = RasterizedP>> {
        let mut vertices = [
            rendering_triangle.0,
            rendering_triangle.1,
            rendering_triangle.2,
        ];
        vertices.sort_by_key(|p| (p.0, p.1));

        // We name each edge after the vertex it's opposite of, for example:
        //     1
        //    / \
        //  [2] [0]
        //  /     \
        // 0--[1]--2
        let e0 = ParametrizedLine::new(Line(vertices[1], vertices[2]));
        let e1 = ParametrizedLine::new(Line(vertices[0], vertices[2]));
        let e2 = ParametrizedLine::new(Line(vertices[0], vertices[1]));

        let slope_denom_1 = vertices[2].0 - vertices[0].0;
        let slope_num_1 = vertices[2].1 - vertices[0].1;
        let slope_denom_2 = vertices[1].0 - vertices[0].0;
        let slope_num_2 = vertices[1].1 - vertices[0].1;

        let iterator = if vertices[0].0 < 0
            || vertices[2].0 > corner.0
            || cmp::max(vertices[2].1, cmp::max(vertices[1].1, vertices[0].1))
                < 0
            || cmp::min(vertices[2].1, cmp::min(vertices[1].1, vertices[0].1))
                > corner.1
            || slope_denom_1 == 0
        {
            // If the triangle is completely outside of the frame, we don't
            // need to generate any points.
            // If edge 1 has infinite slope (then so does edge 2) and the
            // triangle is just a vertical line, so degenerate.
            Box::new(iter::empty())
        } else if slope_denom_2 == 0 {
            if slope_num_2 == 0 {
                // If edge 2 has length 0, then the triangle is degenerate.
                Box::new(iter::empty())
            } else {
                // If edge 2 has infinite slope, then we just need to sweep
                // between edge 1 and edge 0, for example:
                // 1
                // |\
                // | 2
                // |/
                // 0
                sweep_between_slopes(e1, e0, corner)
            }
        } else {
            match slope_num_2 * slope_denom_1 - slope_num_1 * slope_denom_2 {
                n if n < 0 => {
                    // If edge 1 has steeper slope than edge 2, then the lower path is
                    // the longer one, for example:
                    // 0---2
                    //  \ /
                    //   1
                    sweep_between_slopes(e2.chain(e0.skip(1)), e1, corner)
                }
                n if n > 0 => {
                    // If edge 2 has steeper slope than edge 1, the lower path is the
                    // shorter one, for example:
                    //   1
                    //  / \
                    // 0---2
                    sweep_between_slopes(e1, e2.chain(e0.skip(1)), corner)
                }
                _ => {
                    // Otherwise the triagle is degenerate.
                    Box::new(iter::empty())
                }
            }
        };

        Box::new(iterator.map(|rendering_p| RasterizedP {
            rendering_p,
            coords: None,
        }))
    }
}

fn sweep_between_slopes<
    'a,
    I: 'a + Iterator<Item = RenderingP> + Clone,
    J: 'a + Iterator<Item = RenderingP> + Clone,
>(
    mut lower: I,
    mut upper: J,
    corner: P2<i32>,
) -> Box<dyn 'a + Iterator<Item = RenderingP>> {
    let mut iterator: Box<dyn Iterator<Item = RenderingP>> =
        Box::new(iter::empty());
    while let (
        Some(RenderingP(mut lower_x, mut lower_y, mut lower_z)),
        Some(RenderingP(mut upper_x, mut upper_y, mut upper_z)),
    ) = (lower.next(), upper.next())
    {
        while lower_x < upper_x {
            match lower.next() {
                Some(RenderingP(x, y, z)) => {
                    let l = RenderingP(lower_x, lower_y, lower_z);
                    if l.is_in_frame(corner) {
                        iterator = Box::new(iterator.chain(iter::once(l)));
                    }
                    lower_x = x;
                    lower_y = y;
                    lower_z = z;
                }
                None => {
                    break;
                }
            }
        }
        while upper_x < lower_x {
            match upper.next() {
                Some(RenderingP(x, y, z)) => {
                    let u = RenderingP(upper_x, upper_y, upper_z);
                    if u.is_in_frame(corner) {
                        iterator = Box::new(iterator.chain(iter::once(u)));
                    }
                    upper_x = x;
                    upper_y = y;
                    upper_z = z;
                }
                None => {
                    break;
                }
            }
        }
        let x = lower_x;
        if 0 <= x && x <= corner.0 {
            let slope = (upper_z - lower_z) / (upper_y - lower_y) as f32;
            if upper_y > corner.1 {
                upper_y = corner.1;
            }
            if lower_y < 0 {
                lower_z -= slope * (lower_y as f32);
                lower_y = 0;
            }
            iterator = Box::new(iterator.chain((lower_y..=upper_y).scan(
                lower_z,
                move |current_z, y| {
                    let z = *current_z;
                    *current_z += slope;
                    Some(RenderingP(x, y, z))
                },
            )));
        } else {
            break;
        }
    }
    iterator
}
