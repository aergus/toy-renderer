mod bounding_box;
pub use bounding_box::BoundingBox;
mod line_sweeping;
pub use line_sweeping::LineSweeping;

use super::geometry::{Barycentric, RenderingP, Triangle, P2, P4};

pub struct RasterizedP {
    pub rendering_p: RenderingP,
    pub coords: Option<Barycentric>,
}

// The original idea was requiring the rasterizers to return something like an
// `impl Iterator<...>`, or a least parametrizing the rasterizer traits
// over an `I` for which appropriate trait bounds could be imposed while
// using it, so that things are statically dispatched.
// The former isn't allowed (yet?), the latter doesn't work in practice
// (e.g. for the "bounding box rasterizer") because iterator combinators
// which use closures return "anonymous" types which cannot be named.
// Maybe something like this would work with a few adjustments after RFC 2515
// makes it into stable:
// https://stackoverflow.com/a/39490692
// See also:
// https://stackoverflow.com/a/27535594
// https://github.com/rust-lang/rust/issues/34511

pub trait Rasterizer {
    fn rasterize(
        &self,
        triangle: Triangle<P4<f32>>,
        rendering_triangle: Triangle<RenderingP>,
        corner: P2<i32>,
    ) -> Box<dyn Iterator<Item = RasterizedP>>;
}
