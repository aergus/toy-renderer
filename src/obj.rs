use super::geometry::{Transformation, Triangle, P3};

use obj::Obj;

#[derive(Clone, Debug)]
struct VertexTracker {
    sum_of_normals: P3<f32>,
    occurences: Vec<(usize, usize, usize, usize)>,
}

/// Adds a normal vector for the vertices that are missing one.
///
/// The normal vector is calculated by adding up the normals of the triangles
/// which contain the given vertex as one of the vertices and then normalizing
/// the sum.
pub fn with_normals(mut obj: Obj) -> Obj {
    let data = &mut obj.data;
    let mut vertices: Vec<Option<VertexTracker>> =
        vec![None; data.position.len()];

    for (object_index, object) in data.objects.iter().enumerate() {
        for (group_index, group) in object.groups.iter().enumerate() {
            for (polygon_index, polygon) in group.polys.iter().enumerate() {
                let l = polygon.0.len();
                for i in 1..l - 1 {
                    let indices = Triangle(0, i, i + 1).map(|index| {
                        ((polygon.0)[index].2, (polygon.0)[index].0, index)
                    });

                    if indices.0 .0.is_none()
                        || indices.1 .0.is_none()
                        || indices.2 .0.is_none()
                    {
                        let normal = indices
                            .map(|(_, index, _)| P3::from(data.position[index]))
                            .normal()
                            .normalized();

                        indices
                            .into_iter()
                            .map(
                                |(
                                    normal_index,
                                    position_index,
                                    vertex_index,
                                )| {
                                    if normal_index.is_none() {
                                        match &mut vertices[position_index] {
                                            None => {
                                                vertices[position_index] =
                                                    Some(VertexTracker {
                                                        sum_of_normals: normal,
                                                        occurences: vec![(
                                                            object_index,
                                                            group_index,
                                                            polygon_index,
                                                            vertex_index,
                                                        )],
                                                    });
                                            }
                                            Some(vt) => {
                                                vt.sum_of_normals += normal;
                                                vt.occurences.push((
                                                    object_index,
                                                    group_index,
                                                    polygon_index,
                                                    vertex_index,
                                                ));
                                            }
                                        }
                                    }
                                },
                            )
                            .collect()
                    }
                }
            }
        }
    }

    for mut vertex_tracker in vertices.into_iter().flatten() {
        for (object_index, group_index, polygon_index, vertex_index) in
            vertex_tracker.occurences.iter()
        {
            (data.objects[*object_index].groups[*group_index].polys
                [*polygon_index]
                .0)[*vertex_index]
                .2 = Some(data.normal.len());
        }

        let normal = vertex_tracker.sum_of_normals.normalized();
        data.normal.push([normal.0, normal.1, normal.2]);
    }

    obj
}

/// Scales and translates the model so that it (more or less) fits into the
/// view.
pub fn preprocess(obj: &mut Obj) {
    let mut min = P3(f32::INFINITY, f32::INFINITY, f32::INFINITY);
    let mut max = P3(f32::NEG_INFINITY, f32::NEG_INFINITY, f32::NEG_INFINITY);

    for point in obj.data.position.iter() {
        for (m, v) in min.iter_mut().zip(point.iter()) {
            if *m > *v {
                *m = *v;
            }
        }
        for (m, v) in max.iter_mut().zip(point.iter()) {
            if *m < *v {
                *m = *v;
            }
        }
    }

    let shift = ((min + max) / 2.0).map(|v| -v);

    let span = max - min;
    let mut inverse_factor = f32::NEG_INFINITY;
    for v in span.into_iter() {
        if v > inverse_factor {
            inverse_factor = v;
        }
    }
    let factor = 2.0 / inverse_factor;

    let transformation =
        Transformation::scaling(factor) * Transformation::translation(shift);

    for point in obj.data.position.iter_mut() {
        let p = transformation * P3::from(*point);
        *point = [p.0, p.1, p.2];
    }
}
