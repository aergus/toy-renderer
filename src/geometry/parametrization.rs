use super::{Line, RenderingP};

use std::mem;

#[derive(Clone, Debug)]
pub struct ParametrizedLine {
    transpose: bool,
    start: RenderingP,
    end_param: i32,
    param_diff_signum: i32,
    param_diff_abs: i32,
    val_dir: i32,
    scaled_err_incr: i32,
    scaled_err: i32,
    z_diff: f32,
    position: Option<RenderingP>,
}

impl ParametrizedLine {
    pub fn new(mut line: Line<RenderingP>) -> Self {
        let mut transpose = false;

        // We make sure that we run "from left to right".
        if (line.0).0 > (line.1).0 {
            mem::swap(&mut line.0, &mut line.1);
        }

        // The axis for which the difference of the corresponding coordinates
        // is larger in absolute value will be the "parameter axis" and the
        // iterator will generate points by computing the "value-parameter
        // pairs" that (approximately) lie on the line.
        // By default, we will assume that the x axis is the parameter axis,
        // and transpose the input and the output if the y range is
        // greater than the x range.
        if ((line.0).1 as i32 - (line.1).1 as i32).abs()
            > ((line.0).0 as i32 - (line.1).0 as i32).abs()
        {
            line.0.transpose();
            line.1.transpose();
            transpose = true;
        }

        // We want to increment or decrement the y coordinate when
        // `(steps * (val_diff / param_diff)).abs() > 0.5`.
        // This condition is equivalent to
        // `2 * steps * val_diff.abs() > param_diff.abs()`.
        // The latter can be implemented using only integer operations, so we
        // use `scaled_error = steps * scaled_error_incr` where
        // `scaled_error_incr = 2 * val_diff.abs()`.
        let param_diff = (line.1).0 as i32 - (line.0).0 as i32;
        let param_diff_signum = param_diff.signum();
        let param_diff_abs = param_diff.abs();
        let val_diff = (line.1).1 as i32 - (line.0).1 as i32;
        ParametrizedLine {
            transpose,
            start: line.0,
            end_param: line.1 .0,
            param_diff_signum,
            param_diff_abs,
            val_dir: val_diff.signum(),
            scaled_err_incr: 2 * val_diff.abs(),
            scaled_err: 0,
            position: None,
            z_diff: (line.1 .2 - line.0 .2) / (param_diff as f32),
        }
    }
}

impl Iterator for ParametrizedLine {
    type Item = RenderingP;
    fn next(&mut self) -> Option<Self::Item> {
        let mut next = match self.position {
            None => Some(self.start),
            Some(p) => {
                if p.0 == self.end_param {
                    None
                } else {
                    let next_param = p.0 + self.param_diff_signum;
                    self.scaled_err += self.scaled_err_incr;
                    let next_val = if self.scaled_err > self.param_diff_abs {
                        self.scaled_err -= 2 * self.param_diff_abs;
                        p.1 + self.val_dir
                    } else {
                        p.1
                    };
                    let next_z = p.2 + self.z_diff;
                    let next = RenderingP(next_param, next_val, next_z);
                    Some(next)
                }
            }
        }?;
        self.position = Some(next);
        if self.transpose {
            next.transpose();
        }
        Some(next)
    }
}
