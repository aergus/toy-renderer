pub mod parametrization;
pub mod test;

use super::matrix::{Matrix4, ID4, ZERO4};

use derive_more::{
    Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign,
};
use std::cmp::{self, Ord};
use std::convert::From;
use std::iter::{once, Chain, IntoIterator, Once};
use std::mem;
use std::ops::{Add, Mul, Sub};

type Iter2<U> = Chain<Once<U>, Once<U>>;
type Iter3<U> = Chain<Chain<Once<U>, Once<U>>, Once<U>>;
type Iter4<U> = Chain<Chain<Chain<Once<U>, Once<U>>, Once<U>>, Once<U>>;

#[derive(
    Clone,
    Copy,
    PartialEq,
    Eq,
    Debug,
    Add,
    AddAssign,
    Sub,
    SubAssign,
    Mul,
    MulAssign,
    Div,
    DivAssign,
)]
pub struct P2<T>(pub T, pub T);

impl<T: Copy> From<&[T; 2]> for P2<T> {
    fn from(s: &[T; 2]) -> P2<T> {
        P2(s[0], s[1])
    }
}

impl<T> IntoIterator for P2<T> {
    type Item = T;
    type IntoIter = Iter2<T>;

    fn into_iter(self) -> Self::IntoIter {
        once(self.0).chain(once(self.1))
    }
}

impl<T> P2<T> {
    pub fn transpose(&mut self) {
        mem::swap(&mut self.0, &mut self.1)
    }
    pub fn map<S, F>(self, f: F) -> P2<S>
    where
        F: Fn(T) -> S,
    {
        P2(f(self.0), f(self.1))
    }
    pub fn iter(&self) -> Iter2<&T> {
        once(&self.0).chain(once(&self.1))
    }
    pub fn iter_mut(&mut self) -> Iter2<&mut T> {
        once(&mut self.0).chain(once(&mut self.1))
    }
}

pub fn bounding_box<T: Copy + Ord>(
    triangle: Triangle<P2<T>>,
    lower_left: P2<T>,
    upper_right: P2<T>,
) -> (P2<T>, P2<T>) {
    (
        P2(
            cmp::max(
                cmp::min(
                    (triangle.0).0,
                    cmp::min((triangle.1).0, (triangle.2).0),
                ),
                lower_left.0,
            ),
            cmp::max(
                cmp::min(
                    (triangle.0).1,
                    cmp::min((triangle.1).1, (triangle.2).1),
                ),
                lower_left.1,
            ),
        ),
        P2(
            cmp::min(
                cmp::max(
                    (triangle.0).0,
                    cmp::max((triangle.1).0, (triangle.2).0),
                ),
                upper_right.0,
            ),
            cmp::min(
                cmp::max(
                    (triangle.0).1,
                    cmp::max((triangle.1).1, (triangle.2).1),
                ),
                upper_right.1,
            ),
        ),
    )
}

#[derive(
    Clone,
    Copy,
    PartialEq,
    Eq,
    Debug,
    Add,
    AddAssign,
    Sub,
    SubAssign,
    Mul,
    MulAssign,
    Div,
    DivAssign,
)]
pub struct P3<T>(pub T, pub T, pub T);

impl<T: Copy> From<[T; 3]> for P3<T> {
    fn from(s: [T; 3]) -> P3<T> {
        P3(s[0], s[1], s[2])
    }
}

impl<T> IntoIterator for P3<T> {
    type Item = T;
    type IntoIter = Iter3<T>;

    fn into_iter(self) -> Self::IntoIter {
        once(self.0).chain(once(self.1)).chain(once(self.2))
    }
}

impl<T> P3<T> {
    pub fn cross<U, V, W>(&self, rhs: P3<U>) -> P3<W>
    where
        T: Copy,
        U: Copy,
        T: Mul<U, Output = V>,
        V: Sub<Output = W>,
    {
        P3(
            self.1 * rhs.2 - self.2 * rhs.1,
            self.2 * rhs.0 - self.0 * rhs.2,
            self.0 * rhs.1 - self.1 * rhs.0,
        )
    }
    pub fn dot<U, V>(self, rhs: P3<U>) -> V
    where
        T: Mul<U, Output = V>,
        V: Add<Output = V>,
    {
        self.0 * rhs.0 + self.1 * rhs.1 + self.2 * rhs.2
    }
    pub fn map<S, F>(self, f: F) -> P3<S>
    where
        F: Fn(T) -> S,
    {
        P3(f(self.0), f(self.1), f(self.2))
    }
    pub fn iter(&self) -> Iter3<&T> {
        once(&self.0).chain(once(&self.1)).chain(once(&self.2))
    }
    pub fn iter_mut(&mut self) -> Iter3<&mut T> {
        once(&mut self.0)
            .chain(once(&mut self.1))
            .chain(once(&mut self.2))
    }
}

impl P3<f32> {
    pub fn norm(self) -> f32 {
        (self.dot(self)).sqrt()
    }
    pub fn normalize(&mut self) {
        *self /= self.norm();
    }
    pub fn normalized(&mut self) -> Self {
        self.normalize();
        *self
    }
    pub fn for_rendering(self) -> RenderingP {
        RenderingP(self.0.round() as i32, self.1.round() as i32, self.2)
    }
}

pub type Transformation = Matrix4<f32>;

impl Mul<Transformation> for Transformation {
    type Output = Transformation;

    fn mul(self, rhs: Transformation) -> Self::Output {
        let mut result = ZERO4;
        for i in 0..4 {
            for j in 0..4 {
                for k in 0..4 {
                    result[(i, j)] += self[(i, k)] * rhs[(k, j)];
                }
            }
        }
        result
    }
}

#[derive(
    Clone,
    Copy,
    PartialEq,
    Eq,
    Debug,
    Add,
    AddAssign,
    Sub,
    SubAssign,
    Mul,
    MulAssign,
    Div,
    DivAssign,
)]
pub struct P4<T>(pub T, pub T, pub T, pub T);

impl<T> IntoIterator for P4<T> {
    type Item = T;
    type IntoIter = Iter4<T>;

    fn into_iter(self) -> Self::IntoIter {
        once(self.0)
            .chain(once(self.1))
            .chain(once(self.2))
            .chain(once(self.3))
    }
}

impl<T> P4<T> {
    pub fn iter(&self) -> Iter4<&T> {
        once(&self.0)
            .chain(once(&self.1))
            .chain(once(&self.2))
            .chain(once(&self.3))
    }
    pub fn iter_mut(&mut self) -> Iter4<&mut T> {
        once(&mut self.0)
            .chain(once(&mut self.1))
            .chain(once(&mut self.2))
            .chain(once(&mut self.3))
    }
}

impl From<P3<f32>> for P4<f32> {
    fn from(P3(x, y, z): P3<f32>) -> P4<f32> {
        P4(x, y, z, 1.0)
    }
}

impl P4<f32> {
    pub fn perspective_projection(self) -> P3<f32> {
        P3(self.0 / self.3, self.1 / self.3, self.2 / self.3)
    }
}

impl Transformation {
    fn write_3d_row(&mut self, i: usize, v: P3<f32>) {
        self[(i, 0)] = v.0;
        self[(i, 1)] = v.1;
        self[(i, 2)] = v.2;
    }
    pub fn translation(v: P3<f32>) -> Self {
        let mut result = ID4;
        result[(0, 3)] = v.0;
        result[(1, 3)] = v.1;
        result[(2, 3)] = v.2;
        result
    }
    pub fn scaling(s: f32) -> Self {
        let mut result = ID4;
        result[(0, 0)] = s;
        result[(1, 1)] = s;
        result[(2, 2)] = s;
        result
    }
    pub fn projection(cam_z: f32) -> Self {
        let mut result = ID4;
        result[(3, 2)] = -1.0 / cam_z;
        result
    }
    pub fn view(mut camera: P3<f32>, up: P3<f32>) -> Self {
        let third = camera.normalized();
        let first = up.cross(third).normalized();
        let second = third.cross(first).normalized();
        let mut result = ID4;
        result.write_3d_row(0, first);
        result.write_3d_row(1, second);
        result.write_3d_row(2, third);
        result
    }
}

impl Mul<P4<f32>> for Transformation {
    type Output = P4<f32>;

    fn mul(self, rhs: P4<f32>) -> Self::Output {
        let vec = [rhs.0, rhs.1, rhs.2, rhs.3];
        let mut result = [0.0, 0.0, 0.0, 0.0];
        for i in 0..4 {
            for k in 0..4 {
                result[i] += self[(i, k)] * vec[k];
            }
        }
        P4(result[0], result[1], result[2], result[3])
    }
}

impl Mul<P3<f32>> for Transformation {
    type Output = P3<f32>;

    fn mul(self, rhs: P3<f32>) -> Self::Output {
        (self * P4::from(rhs)).perspective_projection()
    }
}

// This exists because "its semantics are different than that of `P3<f32>`".
#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Barycentric(pub f32, pub f32, pub f32);

impl Barycentric {
    pub fn from_scaled(ns: (i32, i32, i32), d: i32) -> Self {
        let d = d as f32;
        Barycentric(ns.0 as f32 / d, ns.1 as f32 / d, ns.2 as f32 / d)
    }
    pub fn correct_perspective(self, triangle: Triangle<P4<f32>>) -> Self {
        let u = self.0 / triangle.0 .3;
        let v = self.1 / triangle.1 .3;
        let w = self.2 / triangle.2 .3;
        let sum = u + v + w;
        Barycentric(u / sum, v / sum, w / sum)
    }
}

impl<T, S> Triangle<T>
where
    T: Copy,
    T: Mul<f32, Output = S>,
    S: Add<S, Output = S>,
{
    pub fn interpolate(&self, coords: Barycentric) -> S {
        self.0 * coords.0 + self.1 * coords.1 + self.2 * coords.2
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Line<P>(pub P, pub P);

impl<T> IntoIterator for Line<T> {
    type Item = T;
    type IntoIter = Iter2<T>;

    fn into_iter(self) -> Self::IntoIter {
        once(self.0).chain(once(self.1))
    }
}

impl<T> Line<T> {
    pub fn map<S, F>(self, f: F) -> Line<S>
    where
        F: Fn(T) -> S,
    {
        Line(f(self.0), f(self.1))
    }
    pub fn iter(&self) -> Iter2<&T> {
        once(&self.0).chain(once(&self.1))
    }
    pub fn iter_mut(&mut self) -> Iter2<&mut T> {
        once(&mut self.0).chain(once(&mut self.1))
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Triangle<P>(pub P, pub P, pub P);

impl<T> IntoIterator for Triangle<T> {
    type Item = T;
    type IntoIter = Iter3<T>;

    fn into_iter(self) -> Self::IntoIter {
        once(self.0).chain(once(self.1)).chain(once(self.2))
    }
}

impl<T> Triangle<T> {
    pub fn map<S, F>(self, f: F) -> Triangle<S>
    where
        F: Fn(T) -> S,
    {
        Triangle(f(self.0), f(self.1), f(self.2))
    }
    pub fn iter(&self) -> Iter3<&T> {
        once(&self.0).chain(once(&self.1)).chain(once(&self.2))
    }
    pub fn iter_mut(&mut self) -> Iter3<&mut T> {
        once(&mut self.0)
            .chain(once(&mut self.1))
            .chain(once(&mut self.2))
    }
}

impl<T: Copy> Triangle<T> {
    pub fn lines(&self) -> Iter3<Line<T>> {
        once(Line(self.1, self.2))
            .chain(once(Line(self.0, self.2)))
            .chain(once(Line(self.0, self.1)))
    }
}

impl<T> Triangle<P3<T>> {
    pub fn normal(&self) -> P3<T>
    where
        T: Copy,
        T: Sub<Output = T>,
        T: Mul<Output = T>,
    {
        (self.2 - self.0).cross(self.1 - self.0)
    }
}

impl Triangle<P2<i32>> {
    pub fn scaled_barycentric(&self, p: P2<i32>) -> ((i32, i32, i32), i32) {
        let P3(x, y, z) = (P3((self.2).0, (self.1).0, (self.0).0)
            - P3((self.0).0, (self.0).0, p.0))
        .cross(
            P3((self.2).1, (self.1).1, (self.0).1)
                - P3((self.0).1, (self.0).1, p.1),
        );
        (
            ((z - x - y) * z.signum(), y * z.signum(), x * z.signum()),
            z.abs(),
        )
    }
    pub fn barycentric(&self, p: P2<i32>) -> Barycentric {
        let P3(x, y, z) = (P3((self.2).0, (self.1).0, (self.0).0)
            - P3((self.0).0, (self.0).0, p.0))
        .cross(
            P3((self.2).1, (self.1).1, (self.0).1)
                - P3((self.0).1, (self.0).1, p.1),
        )
        .map(|a| a as f32);
        Barycentric((z - x - y) / z, y / z, x / z)
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct RenderingP(pub i32, pub i32, pub f32);

impl RenderingP {
    pub fn transpose(&mut self) {
        mem::swap(&mut self.0, &mut self.1)
    }
    pub fn project(self) -> P2<i32> {
        P2(self.0, self.1)
    }
    pub fn is_in_frame(self, corner: P2<i32>) -> bool {
        0 <= self.0 && self.0 <= corner.0 && self.1 <= 0 && self.1 <= corner.1
    }
}
