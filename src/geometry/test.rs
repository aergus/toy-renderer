use super::{Line, RenderingP, Triangle, P2, P3};

use rand::Rng;

impl RenderingP {
    pub fn random(bounding_box: (RenderingP, RenderingP)) -> Self {
        let mut rng = rand::thread_rng();
        RenderingP(
            rng.gen_range(bounding_box.0 .0, bounding_box.1 .0),
            rng.gen_range(bounding_box.0 .1, bounding_box.1 .1),
            rng.gen_range(bounding_box.0 .2, bounding_box.1 .2),
        )
    }
}

impl Line<RenderingP> {
    pub fn random(bounding_box: (RenderingP, RenderingP)) -> Self {
        Line(
            RenderingP::random(bounding_box),
            RenderingP::random(bounding_box),
        )
    }
}

impl Triangle<RenderingP> {
    pub fn random(bounding_box: (RenderingP, RenderingP)) -> Self {
        Triangle(
            RenderingP::random(bounding_box),
            RenderingP::random(bounding_box),
            RenderingP::random(bounding_box),
        )
    }
}

// Oddly specific, but this is what we need for texture coordinates...
impl Triangle<P2<f32>> {
    pub fn random_non_degenerate_in_unit_square() -> Self {
        let mut rng = rand::thread_rng();
        let t2 = Triangle(
            P2(rng.gen_range(0.0, 1.0), rng.gen_range(0.0, 1.0)),
            P2(rng.gen_range(0.0, 1.0), rng.gen_range(0.0, 1.0)),
            P2(rng.gen_range(0.0, 1.0), rng.gen_range(0.0, 1.0)),
        );
        let t3 = t2.map(|P2(x, y)| P3(x, y, 0.0));
        if (t3.1 - t3.0).cross(t3.2 - t3.0).2 != 0.0 {
            t2
        } else {
            Triangle::random_non_degenerate_in_unit_square()
        }
    }
}
