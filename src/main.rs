use toy_renderer::geometry::P3;
use toy_renderer::obj::{preprocess, with_normals};
use toy_renderer::rasterizer::BoundingBox;
use toy_renderer::rendering::{Rendering, RGB};
use toy_renderer::shader::fragment::{
    MonochromaticScale, RainbowScale, ScaleGouraud, TexturedGouraud,
};
use toy_renderer::shader::vertex::Transforming;

use image::imageops;
use image::io::Reader;
use obj::Obj;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "toy-renderer", about = "A toy renderer.")]
struct Options {
    /// Path to the Obj model.
    #[structopt(short, long, env, value_name = "path", parse(from_os_str))]
    model: PathBuf,

    /// Move and scale the model to fit into the rendering frame.
    #[structopt(short, long)]
    preprocess: bool,

    /// Render using an image as the texture.
    #[structopt(
        short,
        long,
        env,
        value_name = "path",
        parse(from_os_str),
        conflicts_with_all = &["color", "rainbow"]
    )]
    texture: Option<PathBuf>,

    /// Render using shades of a single color.
    #[structopt(
        short = "i",
        long,
        env,
        value_name = "triple of RGB values",
        conflicts_with_all = &["texture", "rainbow"]
    )]
    color: Option<RGB>,

    /// Render using a rainbow color scale.
    #[structopt(
        short,
        long,
        conflicts_with_all = &["texture", "color"]
    )]
    rainbow: bool,

    /// Light direction.
    #[structopt(short, long, env, value_name = "triple of floats")]
    light: P3<f32>,

    /// Camera position.
    #[structopt(short, long, env, value_name = "triple of floats")]
    camera: P3<f32>,

    /// Height & width of the rendering (in pixels).
    #[structopt(
        short,
        long,
        env,
        value_name = "non-negative integer",
        conflicts_with_all = &["width", "height"]
    )]
    size: Option<u32>,

    /// Width of the rendering (in pixels).
    #[structopt(
        short,
        long,
        env,
        value_name = "non-negative integer",
        conflicts_with = "size"
    )]
    width: Option<u32>,

    /// Height of the rendering (in pixels).
    #[structopt(
        short,
        long,
        env,
        value_name = "non-negative integer",
        conflicts_with = "size"
    )]
    height: Option<u32>,

    /// Path for the output image – it is passed to the `image` crate, which
    /// derives the file format from the extension, and supports JPEG and PNG
    /// files (as of the writing of this help text).
    #[structopt(short, long, value_name = "path", env, parse(from_os_str))]
    output: PathBuf,
}

fn main() {
    let options = Options::from_args();
    let (width, height) = match options.size {
        Some(size) => (size, size),
        None => (options.width.unwrap(), options.height.unwrap()),
    };

    let mut rendering = Rendering::new(width, height);
    let mut obj = with_normals(Obj::load(options.model).unwrap());
    if options.preprocess {
        preprocess(&mut obj);
    }

    let vertex_shader = Transforming::new(options.camera, width, height);
    let rasterizer = BoundingBox();

    // Assuming that the command line argument processing is working correctly,
    // exactly one of the following three blocks will be run, so it's fine
    // to run `rendering.save` afterwards.

    if let Some(texture) = options.texture {
        let mut texture =
            Reader::open(texture).unwrap().decode().unwrap().into_rgb8();
        imageops::flip_vertical_in_place(&mut texture);
        let fragment_shader = TexturedGouraud::new(&texture, options.light);
        rendering.render_obj(
            &obj,
            &rasterizer,
            &vertex_shader,
            &fragment_shader,
        );
    }

    if let Some(color) = options.color {
        let fragment_shader =
            ScaleGouraud::new(MonochromaticScale(color), options.light);
        rendering.render_obj(
            &obj,
            &rasterizer,
            &vertex_shader,
            &fragment_shader,
        );
    }

    if options.rainbow {
        let fragment_shader = ScaleGouraud::new(RainbowScale(), options.light);
        rendering.render_obj(
            &obj,
            &rasterizer,
            &vertex_shader,
            &fragment_shader,
        );
    }

    rendering.save(options.output).unwrap()
}
